export const environment = {
  production: true,
  apiUrl: 'http://localhost:4200',
  firebase : {
    apiKey: "AIzaSyCdPsBjqHNqK9dd17ZIftPQBpNHqpkRHo0",
    authDomain: "vitality-clinic-fa8b4.firebaseapp.com",
    databaseURL: "https://vitality-clinic-fa8b4-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "vitality-clinic-fa8b4",
    storageBucket: "vitality-clinic-fa8b4.appspot.com",
    messagingSenderId: "911102505085",
    appId: "1:911102505085:web:3486417c55c200fc5a4114",
    measurementId: "G-01FSPFGTZS"
  },
};
