// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: 'http://localhost:4200',
  firebase : {
    apiKey: "AIzaSyCdPsBjqHNqK9dd17ZIftPQBpNHqpkRHo0",
    authDomain: "vitality-clinic-fa8b4.firebaseapp.com",
    databaseURL: "https://vitality-clinic-fa8b4-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "vitality-clinic-fa8b4",
    storageBucket: "vitality-clinic-fa8b4.appspot.com",
    messagingSenderId: "911102505085",
    appId: "1:911102505085:web:3486417c55c200fc5a4114",
    measurementId: "G-01FSPFGTZS"
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
