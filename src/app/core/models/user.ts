import { Role } from './role';

export class User {
  uid: string;
  email: string;
  displayName: string;
  photoURL: string;
  emailVerified: boolean;
  password: string;
  firstName: string;
  lastName: string;
  role: Role;
  token: string;
}
