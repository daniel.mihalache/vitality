import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StaffRoutingModule } from './staff-routing.module';
import { AllstaffComponent } from './allstaff/allstaff.component';
import { FormDialogComponent } from './allstaff/dialog/form-dialog/form-dialog.component';
import { DeleteDialogComponent } from './allstaff/dialog/delete/delete.component';
import { AddStaffComponent } from './add-staff/add-staff.component';
import { StaffService } from './allstaff/staff.service';
import { ComponentsModule } from '@shared/components/components.module';
import { SharedModule } from '@shared';

@NgModule({
  declarations: [
    AllstaffComponent,
    FormDialogComponent,
    DeleteDialogComponent,
    AddStaffComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    StaffRoutingModule,
    ComponentsModule,
    SharedModule,
  ],
  providers: [StaffService],
})
export class StaffModule {}
